package MyTestBackend;

import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class RegistrationTest {
    @Test
    void findXAuthToken() {
        Object response = given()
                .contentType("application/x-www-form-urlencoded")
                .formParam("username", "5155")
                .formParam("password", "2dfe1946b3")
                .when()
                .post("https://test-stand.gb.ru/gateway/login")
                .then().extract()
                .jsonPath()
                .get("token")
                .toString();
        System.out.println("API.Token: " + response);
    }

    @Test
    void noReceiveXAuthTokenWithInvalidPassword() {
        given()
                .contentType("application/x-www-form-urlencoded")
                .formParam("username", "5155")
                .formParam("password", "2dfe1946b333")
                .when()
                .post("https://test-stand.gb.ru/gateway/login")
                .then()
                .statusCode(401);

    }
    @Test
    void noReceiveXAuthTokenWithInvalidUsernameAndPassword() {
        given()
                .contentType("application/x-www-form-urlencoded")
                .formParam("username", "4567")
                .formParam("password", "u76899ghj")
                .when()
                .post("https://test-stand.gb.ru/gateway/login")
                .then()
                .statusCode(401);
    }
    @Test
    void noGetXAuthTokenWithEmptyUsernameAndPassword() {
        given()
                .contentType("application/x-www-form-urlencoded")
                .formParam("username", "")
                .formParam("password", "")
                .when()
                .post("https://test-stand.gb.ru/gateway/login")
                .then()
                .statusCode(401);
    }

}
