package MyTestBackend;

import io.restassured.http.Headers;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.lessThan;

public class MainTest extends SourceTest {
    @Test
    void requestToLookAtOtherPeoplesPostsWithoutSort() {
        JsonPath response = given()
                .spec(requestSpecification)
                .queryParam("", "")
                .when()
                .get(getBaseUrl())
                .jsonPath();
        int id = response.get("data[1].id");
        assertThat(response.get("data[0].id"), lessThan(id));
    }

    @Test
    void requestNoPageNumber() {
        given()
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "10000")
                .when()
                .get(getBaseUrl()+"api/posts")
                .then()
                .spec(responseSpecification);
    }

    @Test
    void requestPostsByPublicationDateByAuthorisation() {
        given()
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "ABC")
                .queryParam("page", "1")
                .when()
                .get(getBaseUrl()+"api/posts")
                .then()
                .spec(responseSpecification);
    }

    @Test
    void requestOtherPeoplePostsAll() {
        given()
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "ALL")
                .when()
                .get(getBaseUrl()+"api/posts")
                .then()
                .spec(responseSpecification);
    }

    @Test
    void requestInvalidPageParameters() {
        given()
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "-3")
                .when()
                .get(getBaseUrl()+"api/posts")
                .then()
                .spec(responseSpecification);
    }

    @Test
    void requestYourPostsInAscendingOrder() {
        given()
                .queryParam("sort", "createdAt")
                .queryParam("order", "")
                .when()
                .get(getBaseUrl()+"api/posts")
                .then()
                .statusCode(200);
    }

    @Test
    void requestToLookMyPostsWithoutSort() {
        given()
                .queryParam("", "")
                .when()
                .get(getBaseUrl()+"api/posts")
                .then()
                .spec(responseSpecification);
    }

    @Test
    void requestNoPage() {
        given()
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "39")
                .when()
                .get(getBaseUrl()+"api/posts")
                .then()
                .spec(responseSpecification);
    }

    @Test
    void requestInvalidOwnPage() {
        given()
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "-3")
                .when()
                .get(getBaseUrl()+"api/posts")
                .then()
                .spec(responseSpecification);
    }

    @Test
    void requestExistingPagegSway() {
        given()
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "1")
                .when()
                .get(getBaseUrl()+"api/posts")
                .then()
                .spec(responseSpecification);
    }

}

